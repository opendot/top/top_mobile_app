import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { detailRequest } from './GameRedux'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  initSocketChannel: [],
  setTrackerData: ['data',],
  sendToTrackerWs:['data',],
  setCalibrationPoints:['calibration_points'],
  setCurrentPoint:['current_point'],
  setCalibrating:['calibrating'],
  setConnected:['connected'],
  stopWebsocket:[],
  setLicensed: ['licensed']
})

export const TrackerSocketTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  calibtation_points:[],
  current_point:[],
  calibrating: false,
  connected: false,
  licensed: false
})

/* ------------- Selectors ------------- */

export const TrackerSocketSelectors = {
  getGaze: state => state.trackerSocket.data,
  getCalibrationPoints: state => state.trackerSocket.calibration_points,
  getCurrentPoint: state => state.trackerSocket.current_point,
  isCalibrating: state => state.trackerSocket.calibrating,
  isConnected: state => state.trackerSocket.connected,
  isLicensed: state => state.trackerSocket.licensed
}

/* ------------- Reducers ------------- */

export const setData = (state, { data }) => {
  return state.merge(data)
}

export const calibrationPoints = (state, action) => {
  const { calibration_points } = action
  return state.merge({ calibration_points })
}

export const currentPoint = (state, action) => {
  const { current_point } = action
  return state.merge({ current_point })
}

export const calibrateStatus = (state, action) => {
  const { calibrating } = action
  return state.merge({ calibrating })
}

export const connectedStatus = (state, action) => {
  const { connected } = action
  console.log("connected!!", connected)
  return state.merge({ connected })
}

export const setLicensed = (state, action) => {
  const { licensed } = action
  return state.merge({ licensed })
}
/*
// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })
*/
/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SET_TRACKER_DATA]: setData,
  [Types.SET_CALIBRATION_POINTS]: calibrationPoints,
  [Types.SET_CURRENT_POINT]: currentPoint,
  [Types.SET_CALIBRATING]: calibrateStatus,
  [Types.SET_CONNECTED]: connectedStatus,
  [Types.SET_LICENSED]: setLicensed,
})
