import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import _ from 'lodash'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  setGameInfo: ['info'],
  setSelectedLevel: ['selectedLevel'],
  setSelectedRound: ['selectedRound'],
  setDynamics: ['level','dynamics_id', 'scene','value'],
  gameRequest: ['data'],
  gameSuccess: ['payload'],
  gameFailure: null,
  setSkipIntro: null,
  setPreviewSession: null,
  newRecordedSession: null,
  recordedSessionSuccess:['session'],
  recordedSessionFail:null,
  checkSessionCorrect: null,
  sessionCorrectSuccess: null,
  sessionCorrectFail: null
})

export const GameTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  details: null,
  levels:null,
  selectedLevel:null,
  selectedRound:null,
  currentScene:null,
  info: null,
  skipintro:false,
  session: null,
  fetching: null,
  error: null,
  sessionCorrect:true
})

/* ------------- Selectors ------------- */

export const GameSelectors = {
  getDetails: state => state.game.details,
  getLevels: state => state.game.levels,
  getInfo: state => state.game.info,
  getSession: state => state.game.session,
  getSkipIntro: state => state.game.skipintro,
  getSelectedLevel: state => state.game.selectedLevel,
  getSelectedRound: state => state.game.selectedRound,
  getSessionCorrect: state => state.game.sessionCorrect,
}

/* ------------- Reducers ------------- */

export const setInfo = (state, { info }) =>
  state.merge({ info })

export const setIntro = (state) =>
  state.merge( {skipintro: !state.skipintro})

export const selectLevel = (state, { selectedLevel }) =>
  state.merge({ selectedLevel })

export const selectRound = (state, { selectedRound }) =>
  state.merge({ selectedRound })

export const setPreviewGameSession = (state) =>
  state.merge({session:"preview"})

export const sessionrequest = (state, action) =>
  state.merge({ fetching: true, session: null })

export const sessionsuccess = (state, action) => {
  const { session } = action
  return state.merge({fetching: false, session: session.id, error:null})
}

export const sessionfailure = (state, action) => {
  const { session } = action
  return state.merge({fetching: false, session: null, error:true})
}

export const updateDynamics = (state, payload) => {
  let lvls = _.cloneDeep(state.levels)
  //let scenes = _(lvls).find((o) =>{return o.index == payload.level}).scenes
  let dynamics = _(lvls).find((p) =>{return p.index == payload.level}).dynamics
  let dyn = _(dynamics).find((q) =>{return q.id == payload.dynamics_id})
  dyn.value = payload.value
  return state.merge({levels:lvls})
}

// request the data from an api
export const detailRequest = (state, { data }) =>
  state.merge({ fetching: true, details: null, levels:null })

// successful api lookup
export const detailSuccess = (state, action) => {
  const { details, levels } = action.payload
  return state.merge({ fetching: false, error: null, details, levels })
}

// Something went wrong somewhere.
export const detailFailure = state =>
  state.merge({ fetching: false, error: true, details: null, levels:null })


// request the data from an api
export const sessionCorrectRequest = (state, { data }) =>
  state.merge({ fetching: true })

// successful api lookup
export const sessionCorrectOk = (state) => {
  return state.merge({ fetching: false, sessionCorrect: true})
}

// Something went wrong somewhere.
export const sessionCorrectKo = state => {
  return state.merge({fetching: false, sessionCorrect: false})
}


/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GAME_REQUEST]: detailRequest,
  [Types.GAME_SUCCESS]: detailSuccess,
  [Types.GAME_FAILURE]: detailFailure,
  [Types.SET_GAME_INFO]: setInfo,
  [Types.SET_SKIP_INTRO]: setIntro,
  [Types.SET_SELECTED_LEVEL]: selectLevel,
  [Types.SET_SELECTED_ROUND]: selectRound,
  [Types.SET_DYNAMICS]: updateDynamics,
  [Types.SET_PREVIEW_SESSION]: setPreviewGameSession,
  [Types.NEW_RECORDED_SESSION]: sessionrequest,
  [Types.RECORDED_SESSION_SUCCESS]: sessionsuccess,
  [Types.RECORDED_SESSION_FAIL]: sessionfailure,
  [Types.CHECK_SESSION_CORRECT]: sessionCorrectRequest,
  [Types.SESSION_CORRECT_SUCCESS]: sessionCorrectOk,
  [Types.SESSION_CORRECT_FAIL]: sessionCorrectKo
})
