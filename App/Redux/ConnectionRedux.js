import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  checkAddress: ['ip','port'],
  storeAddress: ['ip','port'],
  wrongAddress: ['error'],
  changeBaseurl: ['ip','port'],
  loginRequest: ['username','password'],
  loginSuccess: ['user'],
  loginFailure: ['error'],
  patientsRequest: [],
  patientsSuccess: ['patients'],
  patientsFailure: ['error'],
  setPatient: ['patient'],
  gamesRequest:[],
  gamesSuccess:['games'],
  gamesFailure:['error'],
  logout:[],
  startCalibration:[],
  changePatient:[]
})

export const ConnectionTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  patients:[],
  patient:null,
  games: [],
  user:null,
  fetching: null,
  payload: null,
  error: null,
  ip: null,
  port: null,
  eventConnected:false,
  trackerConnected: false,

})

/* ------------- Selectors ------------- */

export const ConnectionSelectors = {
  getToken: state => state.connection.user.token,
  getUser: state => state.connection.user,
  getFullName: state => state.connection.user.complete_name,
  getGames: state => state.connection.games,
  getIp: state => state.connection.ip,
  getError: state => state.connection.error,
  getPort: state => state.connection.port,
  getPatients: state => state.connection.patients,
  getCurrPatient: state => state.connection.patient,

}

/* ------------- Reducers ------------- */

// request the data from an api
export const addressrequest = (state, action) =>
  state.merge({ fetching: true })

// successful api lookup
export const addresssuccess = (state, action) => {
  const { ip, port } = action
  return state.merge({ fetching: false, error: null, ip, port })
}

// Something went wrong somewhere.
export const addressfailure = state =>
  state.merge({ fetching: false, error: true, ip:null, port:null })

// request the data from an api
export const authrequest = (state, action) =>
  state.merge({ fetching: true, payload: null })

// successful api lookup
export const authsuccess = (state, action) => {
  const { user } = action
  return state.merge({ fetching: false, error: null, user })
}

// Something went wrong somewhere.
export const authfailure = state =>
  state.merge({ fetching: false, error: true, user: null })

// request the data from an api
export const patientsrequest = (state) =>
  state.merge({ fetching: true })

// successful api lookup
export const patientssuccess = (state, action) => {
  const { patients } = action
  return state.merge({ fetching: false, error: null, patients })
}

// Something went wrong somewhere.
export const patientsfailure = state =>
  state.merge({ fetching: false, error: true, patients:[] })


export const setpatient = (state, action) => {
  const { patient } = action
  return state.merge({ fetching: false, error: null, patient })
}

export const changepatient = (state) => {
  return state.merge({ patient:null })
}

export const logout = (state) => {
  return state.merge({ user:null })
}

// request the data from an api
export const gamesrequest = (state) =>
  state.merge({ fetching: true })

// successful api lookup
export const gamessuccess = (state, action) => {
  const { games } = action
  return state.merge({ fetching: false, error: null, games })
}

// Something went wrong somewhere.
export const gamesfailure = state =>
  state.merge({ fetching: false, error: true, games:[] })


/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.CHECK_ADDRESS]: addressrequest,
  [Types.STORE_ADDRESS]: addresssuccess,
  [Types.WRONG_ADDRESS]: addressfailure,
  [Types.LOGIN_REQUEST]: authrequest,
  [Types.LOGIN_SUCCESS]: authsuccess,
  [Types.LOGIN_FAILURE]: authfailure,
  [Types.PATIENTS_REQUEST]: patientsrequest,
  [Types.PATIENTS_SUCCESS]: patientssuccess,
  [Types.PATIENTS_FAILURE]: patientsfailure,
  [Types.SET_PATIENT]: setpatient,
  [Types.CHANGE_PATIENT]: changepatient,
  [Types.GAMES_REQUEST]: gamesrequest,
  [Types.GAMES_SUCCESS]: gamessuccess,
  [Types.GAMES_FAILURE]: gamesfailure,
  [Types.LOGOUT]: logout
})
