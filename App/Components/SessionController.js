import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text,BackHandler } from 'react-native'
import styles from './Styles/SessionControllerStyle'
import SessionButton from './SessionButton'
import TrackerSocketActions from '../Redux/TrackerSocketRedux'
import { connect } from 'react-redux'
import { StackActions, NavigationActions } from 'react-navigation';
import { withNavigation } from 'react-navigation';
import GameActions,{ GameSelectors } from '../Redux/GameRedux'

class SessionController extends Component {

  constructor(props){
    super(props);
    this.state = {trackerOn:true}
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.endSession);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.endSession);
  }

  navigateToMainActivity() {
    const resetAction = StackActions.reset({
      index: 1,
      actions: [
        NavigationActions.navigate({routeName: 'MainScreen'}),
        NavigationActions.navigate({routeName: 'GameDetailScreen'}),
      ],
    });
    this.props.navigation.dispatch(resetAction);
  }

  endSession = () => {
    
    this.props.send(JSON.stringify({
      type: "event",
      event_type: "end_session",
      data:1
    }))
    this.props.sendTrackerWs({
      type: "end_session",
      data:1
    })
    this.navigateToMainActivity()
    if(this.props.session != "preview"){
      this.props.checkSessionCorrect();
    }
  }

  render () {
    return (
      <View style={styles.container}>
        <SessionButton
          isOn={this.state.trackerOn}
          sendOn={()=>{
            this.setState({trackerOn : false})
            this.props.send(JSON.stringify({
              type: "event",
              event_type: "toggle_tracker",
              data: 0
            }))
          }}
          sendOff={()=>{
            this.setState({trackerOn : true})
            this.props.send(JSON.stringify({
              type: "event",
              event_type: "toggle_tracker",
              data: 1
            }))
          }}
          onicon={"eye-slash"}
          officon={"eye"}
          ontext={"disattiva eyetracker"}
          offtext={"attiva eyetracker"}
        />

        <SessionButton
          isOn={true}
          sendOn={()=>{
            this.props.send(JSON.stringify({
              type: "event",
              event_type: "attention_sig",
              data:1
            }))
          }}
          onicon={"exclamation-triangle"}
          ontext={"richiamo dell'attenzione"}
        />

        <SessionButton
          isOn={true}
          sendOn={this.endSession}
          onicon={"stop-circle"}
          ontext={"termina sessione"}
        />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {

  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    sendTrackerWs: (data) => dispatch(TrackerSocketActions.sendToTrackerWs(data)),
    checkSessionCorrect: () => dispatch(GameActions.checkSessionCorrect())
  }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(SessionController))
