import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text, Switch } from 'react-native'
import styles from './Styles/BooleanDynamicStyle'
import Slider from "@react-native-community/slider"
import Colors from '../Themes/Colors'

export default class BooleanDynamic extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    return (
      <View style={{marginTop: 10, flex:1, alignItems:"flex-start", justifyContent:"flex-start"}}>
        <Text>{this.props.name}</Text>
        <Switch style={{marginLeft:5, selfAlign:"left"}}
                onValueChange={this.props.onSlideEnd}
                value={this.props.value}
                trackColor={Colors.brand}/>

      </View>
    )
  }
}
