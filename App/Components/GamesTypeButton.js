import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { Image, Text, TouchableOpacity } from 'react-native'
import styles from './Styles/GamesTypeButtonStyle'
import Colors from '../Themes/Colors'

export default class GamesTypeButton extends Component {

  render () {
    return (
      <TouchableOpacity style={[styles.container, {'backgroundColor': this.props.selected ? Colors.selected : "transparent"}]} onPress={this.props.onSelect}>
        <Image style={{borderRadius:20,width:70, height:70, marginRight:20}} source={this.props.icon}/>
        <Text style={{fontSize:16}}>{this.props.text}</Text>
      </TouchableOpacity>
    )
  }
}
