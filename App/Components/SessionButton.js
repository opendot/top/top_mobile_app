import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity } from 'react-native'
import styles from './Styles/SessionButtonStyle'
import Colors from '../Themes/Colors'
import Icon from 'react-native-vector-icons/FontAwesome5'

export default class SessionButton extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    return (
      <View style={styles.container}>
        <TouchableOpacity disabled={this.props.disabled} onPress={this.props.isOn ? this.props.sendOn : this.props.sendOff}>
        <Icon style={{marginBottom:10,textAlign:"center",fontSize:20}} name={this.props.isOn ? this.props.onicon : this.props.officon} color={Colors.darkred}/>
        <Text>{this.props.isOn ? this.props.ontext : this.props.offtext}</Text>
        </TouchableOpacity>
      </View>
    )
  }
}
