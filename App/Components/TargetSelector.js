import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text, Image, TouchableOpacity } from 'react-native'
import styles from './Styles/TargetSelectorStyle'
import Colors from '../Themes/Colors'

export default class TargetSelector extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    return (
      <View style={[styles.container]}>
        <TouchableOpacity style={{flex:1, flexDirection:'row', justifyContent:"flex-start", marginTop:5, alignItems:"center"}} onPress={this.props.onSelect}>

            <Image source={this.props.image} style={{width:30, height:30, resizeMode:'contain',marginRight:10}} />
            <View>
              <Text style={{fontSize:14, fontWeight:'bold'}}>{this.props.text}</Text>
              <Text style={{fontSize:12}}>{this.props.subtext}</Text>
            </View>
          {this.props.selected && <View style={{backgroundColor:Colors.brand, width:20, height:20, borderRadius:10, position:'absolute', right:20}}></View>}

        </TouchableOpacity>
      </View>
    )
  }
}
