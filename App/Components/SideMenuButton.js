import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
import styles from './Styles/SideMenuButtonStyle'
import Colors from '../Themes/Colors'

export default class SideMenuButton extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    return (
      <View style={styles.container}>
        <Icon.Button
        name={this.props.icon}
        backgroundColor="#ffffff"
        onPress={this.props.onSelect}
        color={Colors.charcoal}
        iconStyle={{color:Colors.darkred}}
        style={{borderColor:'#ffffff', fontWeight:400}}
        >{this.props.text}</Icon.Button>
        {this.props.subtext && <Text style={{fontSize:12, paddingLeft:38, color:Colors.cloud}}>{this.props.subtext}</Text>}
      </View>
    )
  }
}
