import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text } from 'react-native'
import styles from './Styles/GazePointerStyle'

export default class GazePointer extends Component {


  render () {
    return (
      <View style={[styles.container, {top:this.props.y*100+"%",left:this.props.x*100+"%"}]}>
      </View>
    )
  }
}
