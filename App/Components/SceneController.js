import React, { Component } from 'react'
import { ScrollView, View, Text, TouchableOpacity, Dimensions } from 'react-native'
import styles from './Styles/SceneControllerStyle'
import TargetSelector from './TargetSelector'
import SessionDynamic from './SessionDynamic'
import { GameSelectors } from '../Redux/GameRedux'
import GameActions from '../Redux/GameRedux'
import { connect } from 'react-redux'
import _ from 'lodash'
import Icon from 'react-native-vector-icons/FontAwesome5'
import BooleanDynamic from './BooleanDynamic'
import PositionChecker from './PositionChecker'
import Colors from '../Themes/Colors'
import { TrackerSocketSelectors } from '../Redux/TrackerSocketRedux'

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

class SceneController extends Component {


  constructor(props) {
    super(props);
    // Non chiamre this.setState() qui!
    this.state = { toggleTracker: false };
  }

  render () {
    let lvl = _(this.props.levels).find((l)=>{return l.index == this.props.selectedLevel})
    let scns = lvl.scenes
    let scn = _(scns).find((s)=>{return s.index == this.props.scene})
    return (
      <View style={styles.container}>

        <View style={{borderBottomWidth:1, borderBottomColor:Colors.frost, flexDirection:"row", height:30}}>
          <TouchableOpacity style={{backgroundColor:this.state.toggleTracker ? "#FFFFFF" : Colors.brand, flex:1, alignItmes:"center", justifyContent:"center"}} onPress={() => {this.setState({toggleTracker:false})}}>
            <Text style={{textAlign: 'center',color:this.state.toggleTracker ? Colors.frost : "#FFFFFF"}}>Gioco</Text>
          </TouchableOpacity>

          {this.props.isLicensed && <TouchableOpacity style={{backgroundColor:this.state.toggleTracker ? Colors.brand : "#FFFFFF", flex:1, alignItmes:"center", justifyContent:"center"}} onPress={() => {this.setState({toggleTracker:true})}}>
            <Text style={{textAlign: 'center',color:this.state.toggleTracker ? "#FFFFFF" : Colors.frost}}>Tracker</Text>
          </TouchableOpacity>}
        </View>

        {!this.state.toggleTracker &&

        <View style={{width:100+"%", padding:10}}>
          <Text style={{fontSize: 16, fontWeight: 'bold'}}>Lista target</Text>
          <ScrollView style={{height: height * 0.25}}>
            {scn.targets != null && scn.targets.map((o) => {
              const sel = this.props.selTarget == o.name
              return (
                <TargetSelector
                  selected={sel}
                  onSelect={() => {this.props.onTargetPress(o.name)}}
                  key={o.name}
                  image={{uri: this.props.baseurl + this.props.game.url + "img/" + o.thumb}}
                  text={o.name}
                  subtext={o.type}/>
              )
            })}
          </ScrollView>

          <Text style={{fontSize: 16, fontWeight: 'bold', marginTop: 20}}>Dinamiche</Text>
          <ScrollView style={{height: height * 0.25}}>
            {lvl.dynamics != null && lvl.dynamics.filter(d => d.id !== 'backScene' && d.id !== 'nextScene').map((d) => {
              console.log(d);
              if (d.type == "number") return (

                <SessionDynamic key={"d_" + d.id}
                                width={"100%"}
                                name={d.name}
                                type={d.type}
                                id={d.id}
                                min={d.min}
                                max={d.max}
                                step={d.step}
                                onSlideEnd={(val) => {
                                  this.props.onChangeDynamics({id: d.id, value: val})
                                  return this.props.updateDynamics(this.props.selectedLevel, d.id, this.props.scene, val)
                                }}
                                value={d.value}/>

              )
              else return (
                <BooleanDynamic
                  key={"d_" + d.id}
                  width={"100%"}
                  name={d.name}
                  type={d.type}
                  id={d.id}
                  onSlideEnd={(val) => {
                    this.props.onChangeDynamics({id: d.id, value: val})
                    return this.props.updateDynamics(this.props.selectedLevel, d.id, this.props.scene, val)
                  }}
                  value={d.value}
                />
              )
            })}
          </ScrollView>

          {this.props.info.type == "cognitive" &&
          <View style={{flex: 1, flexDirection: "row", justifyContent: "center", alignItems: "center", marginTop: 10, padding:10}}>
            <TouchableOpacity disabled={!this.props.buttons.cooldown} onPress={this.props.fatigue}
                              style={this.props.buttons.cooldown ? styles.nextback : styles.nextback_dis}>
              <Text style={{
                textAlign: "center",
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
              }}>{"Attiva/disattiva defaticamento"}</Text>
            </TouchableOpacity>
          </View>}

          <View style={{flex: 1, flexDirection: "row", justifyContent: "center", marginTop: 10}}>
            <TouchableOpacity disabled={!this.props.buttons.back} onPress={this.props.onBack}
                              style={this.props.buttons.back ? styles.nextback : styles.nextback_dis}>
              <Icon name={"chevron-left"}/>
              <Text>{"BACK"}</Text>
            </TouchableOpacity>

            <TouchableOpacity disabled={!this.props.buttons.next} onPress={this.props.onNext}
                              style={this.props.buttons.next ? styles.nextback : styles.nextback_dis}>
              <Text>{"NEXT"}</Text>
              <Icon name={"chevron-right"}/>
            </TouchableOpacity>
          </View>
        </View>
        }

        {this.state.toggleTracker &&
        <View>
          <PositionChecker
            width={width*0.2}
            height={150}
            pointerSize={20}
          >
          </PositionChecker>
        </View>
        }


      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    selectedLevel: GameSelectors.getSelectedLevel(state),
    levels:GameSelectors.getLevels(state),
    info: GameSelectors.getInfo(state),
    isLicensed: TrackerSocketSelectors.isLicensed(state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    updateDynamics: (lvl, dyn_id, scene, val) => dispatch(GameActions.setDynamics(lvl, dyn_id, scene, val)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SceneController)

