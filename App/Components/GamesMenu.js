import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text } from 'react-native'
import styles from './Styles/GamesMenuStyle'
import GamesTypeButton from './GamesTypeButton'
import { connect } from 'react-redux'
const trainingIcon = require("../Images/icon_training.png")
const cognitiveIcon = require("../Images/icon_cognitive.png")
const entertainmentIcon = require("../Images/icon_entertainment.png")
import ConnectionActions,{ ConnectionSelectors } from '../Redux/ConnectionRedux'
import GameActions,{ GameSelectors } from '../Redux/GameRedux'

class GamesMenu extends Component {

  constructor(props){
    super(props)
    this.state = {
      gameType:null
    }
  }

  listGames () {

    let currList = this.props.games.filter((o)=>{return o.type == this.state.gameType})
      return currList.map((obj,i)=>{
        return(
        <GamesTypeButton key={obj.title} text={obj.title} onSelect={()=>{this.props.setInfo(obj)}} icon={{uri:"http://"+this.props.ip+":"+this.props.port+obj.image}}/>
      )
    })
  }

  render () {
    return (
      <View style={styles.container}>
        <View style={styles.gameTypeContainer}>
          <GamesTypeButton text={"Giochi di allenamento"} onSelect={()=> {this.setState({gameType:"training"})}} icon={trainingIcon} selected={this.state.gameType == "training"}/>
          <GamesTypeButton text={"Giochi cognitivi"} onSelect={()=> {this.setState({gameType:"cognitive"})}} icon={cognitiveIcon} selected={this.state.gameType == "cognitive"}/>
          <GamesTypeButton text={"Giochi di intrattenimento"} onSelect={()=> {this.setState({gameType:"entertainment"})}} icon={entertainmentIcon} selected={this.state.gameType == "entertainment"}/>
        </View>
        {(this.props.games && this.state.gameType) && <View style={styles.gameList}>
          {this.listGames()}
        </View>}
      </View>
    )
  }
}


const mapStateToProps = (state) => {
  return {
    ip: ConnectionSelectors.getIp(state),
    port: ConnectionSelectors.getPort(state),
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setInfo: (obj) => dispatch(GameActions.setGameInfo(obj))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GamesMenu)
