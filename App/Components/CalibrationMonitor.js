import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text, Dimensions } from 'react-native'
import styles from './Styles/CalibrationMonitorStyle'
import GazePointer from './GazePointer'

export default class CalibrationMonitor extends Component {

  constructor(props){
    super(props)
    this.height = Dimensions.get("window").height * 0.8;
    this.width = Dimensions.get("window").height * 0.8 * (16/9);
    this.state = {calibrated:false}
    console.log("dims", this.width, this.height)

  }

  componentWillReceiveProps (nextProps, nextContext){
    if(!nextProps.calibrating && this.props.calibrating ){
      console.log("trigger change")
      this.setState({calibrated:true})
    }
  }

  clamp = (n) => {
    if(n<0) n = 0
    else if(n>=1) n = 1
    return n
  }

  render () {

    return (
      <View style={styles.container}>
        {(this.props.points && this.props.calibrating) && this.props.points.map((d,i)=>{
          console.log(d);
          return (<View key={"p"+i} style={{width:30,height:30,position:"absolute", left:d[0]*this.width - 15, top:d[1]*this.height - 15, backgroundColor:"#e83a51", borderRadius:15}}>

          </View>)
        })}

        {(this.props.currPoint && this.props.calibrating) &&
          <View style={{width:50,height:50,position:"absolute", left:this.props.currPoint[0]*this.width - 25, top:this.props.currPoint[1]*this.height - 25, backgroundColor:"#2cb2bf", borderRadius:25}}>
          </View>
        }

        {(this.props.calibrating && this.props.gaze)  && <GazePointer x={this.clamp(+this.props.gaze[0])} y={this.clamp(+this.props.gaze[1])}/>}

        {this.state.calibrated &&
          <View style={{alignSelf:"center", top:200}}>
            <Text style={{color:"#ffffff", fontSize:20}}>
              Calibrazione completata
            </Text>
          </View>
        }


      </View>
    )
  }
}
