import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text } from 'react-native'
import styles from './Styles/PositionCheckerStyle'
import { connect } from 'react-redux'
import { TrackerSocketSelectors } from '../Redux/TrackerSocketRedux'
import Dimensions from 'Dimensions';
const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

let percentColors = [
  { pct: 0.0, color: { r: 0x00, g: 0xff, b: 0 } },
  { pct: 0.5, color: { r: 0xff, g: 0xff, b: 0 } },
  { pct: 1.0, color: { r: 0xff, g: 0x00, b: 0 } } ];

function getColorForPercentage(pct) {
  for (var i = 1; i < percentColors.length - 1; i++) {
    if (pct < percentColors[i].pct) {
      break;
    }
  }
  var lower = percentColors[i - 1];
  var upper = percentColors[i];
  var range = upper.pct - lower.pct;
  var rangePct = (pct - lower.pct) / range;
  var pctLower = 1 - rangePct;
  var pctUpper = rangePct;
  var color = {
    r: Math.floor(lower.color.r * pctLower + upper.color.r * pctUpper),
    g: Math.floor(lower.color.g * pctLower + upper.color.g * pctUpper),
    b: Math.floor(lower.color.b * pctLower + upper.color.b * pctUpper)
  };
  return 'rgb(' + [color.r, color.g, color.b].join(',') + ')';
  // or output as hex if preferred
}


class PositionChecker extends Component {


  computeColor(val){
      let n = Math.abs(0.5 - val)
      if (n > 0.5) n = 0.5;
      if (n < 0) n = 0;
      /*let r = 255 * n * 2;
      let g = 255 * (1 - n * 2);
      return "rgb("+r+","+g+",0)"*/
      return getColorForPercentage(n*2)
  }

  computeSize(val){
    return 1.4-val
  }

  render () {
    let g = this.props.gaze;
    let h = this.props.height;
    let w = this.props.width;
    let p = this.props.pointerSize;
    return (
      <View>
        <Text style={{margin:10}}>Controllo della postura </Text>
      <View style={{
        width:w,
        height:h,
        backgroundColor:"#000000"}}>
        {/*<View style={styles.l_eye}></View>
        <View style={styles.r_eye}></View>*/}

        {(g) &&
        <View>
          {g[4] && <View style={{position:"absolute", top:g[4]*h, right:w*g[3] - (p/2)*this.computeSize(g[5]), width:p*this.computeSize(g[5]), height:p*this.computeSize(g[5]), backgroundColor:this.computeColor(g[5]), borderRadius:(p/2)*this.computeSize(g[5])}}></View>}
          {g[7] && <View style={{position:"absolute", top:g[7]*h, right:w*g[6] - (p/2)*this.computeSize(g[8]), width:p*this.computeSize(g[8]), height:p*this.computeSize(g[8]), backgroundColor:this.computeColor(g[8]), borderRadius:(p/2)*this.computeSize(g[8])}}></View>}
        </View>
        }
      </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    gaze:TrackerSocketSelectors.getGaze(state),
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PositionChecker)
