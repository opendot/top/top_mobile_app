import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text } from 'react-native'
import styles from './Styles/SideMenuStyle'
import ConnectionActions, { ConnectionSelectors } from '../Redux/ConnectionRedux'
import { connect } from 'react-redux'
import Colors from '../Themes/Colors'
import SideMenuButton from './SideMenuButton'
import GameActions from '../Redux/GameRedux'
import TrackerSocketActions, { TrackerSocketSelectors } from '../Redux/TrackerSocketRedux'
import { NavigationActions } from 'react-navigation';
import Config from 'react-native-config';

class SideMenu extends Component {

  constructor(props){
    super(props)
  }
  componentDidMount(){

  }

  render () {
    let showTerapistButtons = !(this.props.user !== null ? this.props.user.guest : false);
    return (
      <View style={styles.container}>
        {showTerapistButtons && <View style={{marginBottom:40,textAlign:'center', height:70, width:'80%',borderBottomWidth:1, borderColor:Colors.frost}}>
          <Text style={{fontSize:14}}>{'Care-receiver'}</Text>
          {this.props.currentPatient && <Text style={{fontSize:20,marginTop:5}}>{this.props.currentPatient.name + " "+ this.props.currentPatient.last_name}</Text>}
        </View>}

        {showTerapistButtons && <SideMenuButton
          icon={'users'}
          text={'Cambia care-receiver'}
          onSelect = {this.props.changePatient}
        />}

        {showTerapistButtons && <SideMenuButton
          icon={'sync-alt'}
          text={'Sincronizzazione'}
          onSelect = {() =>{console.log("pressed sync")}}
          subtext = {"ultima soncronizzazione 12/12/2019 12:30"}
        />}

        {showTerapistButtons && <SideMenuButton
          icon={'stopwatch'}
          text={'Valutazione'}
          onSelect = {() =>{this.props.setInfo(this.props.evaluation)}}
        />}

        {(this.props.user !== null ? this.props.isLicensed : false) && <SideMenuButton
          icon={'user-cog'}
          text={'Calibrazione'}
          onSelect = {
            () =>{
              //this.props.sendTrackerWs({type: "run_calibration"})
              //console.log("going to calibration Screen")
              this.props.startCalibration();
            }}
        />}

        {Config.APP_VERSION !== 'family' &&  <SideMenuButton
          icon={'stop'}
          text={'Disconnettiti'}
          onSelect = {this.props.logout}
        />}

        <SideMenuButton
          icon={'sign-out-alt'}
          text={'Chiudi finestra PC'}
          onSelect = {() =>{
            console.log("pressed quit")
            NavigationActions.navigate({ routeName: 'LoginScreen' })
            this.props.sendTrackerWs({type: 'terminate'})
          }}
        />

      </View>
    )
  }
}


const mapStateToProps = (state) => {
  //console.log(state)
  return {
    currentPatient: ConnectionSelectors.getCurrPatient(state),
    user: ConnectionSelectors.getUser(state),
    isLicensed: TrackerSocketSelectors.isLicensed(state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => dispatch(ConnectionActions.logout()),
    changePatient: () => dispatch(ConnectionActions.changePatient()),
    setInfo: (obj) => dispatch(GameActions.setGameInfo(obj)),
    startCalibration: () => dispatch(ConnectionActions.startCalibration()),
    sendTrackerWs: data => dispatch({type: 'SEND_TO_TRACKER_WS', data})
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SideMenu)
