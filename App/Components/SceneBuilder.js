import React, { Component } from 'react'
import { View, Text, Image, Dimensions,ImageBackground } from 'react-native'
import styles from './Styles/SceneBuilderStyle'
import { TrackerSocketSelectors } from '../Redux/TrackerSocketRedux'
import { connect } from 'react-redux'
import GazePointer from './GazePointer'
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
import _ from 'lodash'
import Target from './Target'

class SceneBuilder extends Component {

  constructor(props) {
    super(props);
    this.ratio = width / this.props.res[0] *0.8;
  }

  renderTarget = (t,i) => {
    if (!t.hidden) {
      switch (t.img) {
        case "rect":
          return this.renderRect(t, i);
        case "ellipse":
          return this.renderEllipse(t, i);
        default:
          return this.renderImage(t, i);
      }
    }
  }

  clamp = (n) => {
    if(n<0) n = 0
    else if(n>=1) n = 1
    return n
  }

  renderRect = (t,i) => {
    return (<View
      key={"rect"+i} style={{...styles.target, width:t.sizX*this.ratio, height:t.sizY*this.ratio, top:(t.posY-t.sizY/2)*this.ratio, left:(t.posX-t.sizX/2)*this.ratio, backgroundColor:t.col,transform: [{ rotate: t.rot+'rad'}]}}
    />);
  }

  renderEllipse = (t,i) => {
    return (<View
      key={"rect"+i} style={{...styles.target, borderRadius:t.sizX/2*this.ratio,width:t.sizX*this.ratio, height:t.sizY*this.ratio, top:(t.posY-t.sizY/2)*this.ratio, left:(t.posX-t.sizX/2)*this.ratio, backgroundColor:t.col,transform: [{ rotate: t.rot+'rad'}]}}
    />);
  }

  renderImage = (t,i) => {
    return (<Image
      key={"img"+i}
      fadeDuration={0}
      source={{uri:this.props.baseurl + (!t.img.includes('char') ? (this.props.game.url +"img/") : ("/app/imgs/characters/")) + (t.char == undefined ? t.img : t.img.replace('char','char-'+t.char))}}
      style={{...styles.target, width:t.sizX*this.ratio, height:t.sizY*this.ratio, top:(t.posY-t.sizY/2)*this.ratio, left:(t.posX-t.sizX/2)*this.ratio,transform: [{ rotate: t.rot+'rad'}]}}
    />)
  }

  orderAssets = () => {
    let ts = this.props.targets ? this.props.targets.map((t)=>{return {...t, type:"t"}}) : []
    let ds = this.props.distractors ? this.props.distractors.map((d)=>{return {...d, type:"d"}}) : []
    let assets = ts.concat(ds)
    assets = assets.map((t)=>{return _.defaults(t, {zIndex:0}) })
    assets = _.orderBy(assets, ['zIndex','type'],['asc',this.props.details.drawTargetsFirst ? 'desc' : 'asc'])
    return assets
  }

  componentDidUpdate(prevProps) {
    if (this.props.res[0] != prevProps.res[0] || this.props.res[1] != prevProps.res[1]) {
      this.ratio = width / this.props.res[0] *0.8;
    }
  }

  render () {

    let assets = this.orderAssets();


    return (

      <View
        onStartShouldSetResponder={(ev) => true}
        onResponderGrant={(ev)=>{
          this.props.send("create",{x:ev.nativeEvent.locationX/(width*0.8), y:ev.nativeEvent.locationY/(width*0.8*9/16)})
        }}
        onResponderReject={(ev)=>{
        }}
      >
      <ImageBackground
        source={{
          uri:this.props.background && !this.props.background.startsWith("#") ? this.props.baseurl+this.props.game.url +"img/"+ this.props.background : null
        }}
        style={{
          position:"absolute",
          top:0,
          left:0,
          width:width*0.8,
          height:width*0.8*9/16,
          backgroundColor:this.props.background && this.props.background.startsWith("#") ? this.props.background : "#333333"}}
      >

        {assets.map((t, i) => {
          if(t.type==='t') return (<Target width={width*0.8} height={width*0.8*9/16} baseurl={this.props.baseurl+ (!t.img.includes('char') ? (this.props.game.url +"img/") : ("/app/imgs/characters/"))} key={t.id} send={this.props.send} object={t} ratio={this.ratio}></Target>)
          // Draw the element. Also pass key
          else return this.renderTarget(t,i);
        })}


        {this.props.gaze && <GazePointer x={this.clamp(+this.props.gaze[0])} y={this.clamp(+this.props.gaze[1])}/>}
      </ImageBackground>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    gaze:TrackerSocketSelectors.getGaze(state),
  }
}

const mapDispatchToProps = (dispatch) => {
  return {

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SceneBuilder)
