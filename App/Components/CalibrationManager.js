import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, Dimensions } from 'react-native'
import styles from './Styles/CalibrationManagerStyle'
import Colors from '../Themes/Colors'

export default class CalibrationManager extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }


  render () {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={{margin:20, padding:10, borderWidth:1, borderRadius:10, borderColor:Colors.brand}}
          onPress={() => {this.props.onBack()}}
        >
          <Text style={{textAlign:'center'}}>{this.props.calibrating ? "Interrompi calibrazione" : "Torna al menu"}</Text>
        </TouchableOpacity>
      </View>
    )
  }
}
