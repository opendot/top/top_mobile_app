import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text, Image, PanResponder } from 'react-native'
import styles from './Styles/TargetStyle'
import _ from "lodash"
import Colors from '../Themes/Colors'

export default class Target extends Component {

  constructor(props){
    super(props)
    this.throttling=false
    this.throttleTime=50
  }

  componentWillMount() {
    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
      //onPanResponderGrant: (evt, gestureState) => {this.selected=true},
      onPanResponderMove:this.throttledMove,
      onPanResponderRelease: (evt, gestureState) => {
        //this.selected=false;
        if(Math.abs(gestureState.dx) < 0.1 && Math.abs(gestureState.dy) < 0.1){
          this.props.send("touch",this.props.object)
        }
      }
    })

  }

  renderTarget = (t) => {
    if(!t.hidden) {
      switch (t.img) {
        case "rect":
          return this.renderRect(t);
          break;
        case "ellipse":
          return this.renderEllipse(t);
          break;
        default:
          return this.renderImage(t);
          break;
      }
    }
  }

  throttledMove = (e,gs) => {

    if(!this.throttling) {

      this.throttling = true
      this.props.send("move", {id: this.props.object.id, x: gs.moveX/this.props.width, y: gs.moveY/this.props.height})
      setTimeout(() => {this.throttling = false}, this.throttleTime)
    }
    else{

    }

    }


  renderRect = (t) => {
    return (<View
      style={{...styles.target, width:t.sizX*this.props.ratio, height:t.sizY*this.props.ratio, top:(t.posY-t.sizY/2)*this.props.ratio, left:(t.posX-t.sizX/2)*this.props.ratio, backgroundColor:t.col,transform: [{ rotate: t.rot+'rad'}]}}
    />);
  }

  renderEllipse = (t) => {
    return (<View
      style={{...styles.target, borderRadius:t.sizX/2*this.props.ratio,width:t.sizX*this.props.ratio, height:t.sizY*this.props.ratio, top:(t.posY-t.sizY/2)*this.props.ratio, left:(t.posX-t.sizX/2)*this.props.ratio, backgroundColor:t.col,transform: [{ rotate: t.rot+'rad'}]}}
    />);
  }
//
  renderImage = (t) => {

    //console.log({width:t.sizX*this.ratio, height:t.sizY*this.ratio, top:(t.posY-t.sizY/2)*this.ratio, left:(t.posX-t.sizX/2)*this.ratio})
    return (<Image
      source={{uri:this.props.baseurl + (t.char == undefined ? t.img : t.img.replace('char','char-'+t.char))}}
      fadeDuration={0}
      style={{...styles.target, width:t.sizX*this.props.ratio, height:t.sizY*this.props.ratio, top:(t.posY-t.sizY/2)*this.props.ratio, left:(t.posX-t.sizX/2)*this.props.ratio,transform: [{ rotate: t.rot+'rad'}]}}
    />)

  }
  
  render () {
    return (
      <View {...this._panResponder.panHandlers}>
        {this.renderTarget(this.props.object)}
      </View>
    )
  }
}
