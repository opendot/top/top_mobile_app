import { StyleSheet } from 'react-native'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
  container: {
    flexDirection:"row",
    justifyContent:"flex-start",
    alignItems:"center",
    width:"100%",
    padding: 40,
    height: "25%"
  }
})
