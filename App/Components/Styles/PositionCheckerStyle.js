import { StyleSheet } from 'react-native'
import Dimensions from 'Dimensions';
const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

export default StyleSheet.create({
  container: {

  },
  l_eye: {
    position:"absolute",
    top:40,
    left:width*0.2*0.4 - 10,
    borderWidth:1,
    borderColor:"#ffffff",
    borderRadius:10,
    width:20,
    height:20
  },
  r_eye: {
    position:"absolute",
    top:40,
    left:width*0.2*0.6 - 10,
    borderWidth:1,
    borderColor:"#ffffff",
    borderRadius:10,
    width:20,
    height:20
  }
})



