import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  target:{
    position:"absolute",
    zIndex:1,
    resizeMode:"contain"
  }
})
