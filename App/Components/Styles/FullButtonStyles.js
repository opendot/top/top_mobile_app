import { StyleSheet } from 'react-native'
import { Fonts, Colors } from '../../Themes/'

export default StyleSheet.create({
  button: {
    marginVertical: 5,
    borderWidth: 1,
    borderColor:Colors.text
  },
  buttonText: {
    margin: 18,
    textAlign: 'center',
    color: Colors.text,
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.bold
  }
})
