import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:"#333333"
  },
  target:{
    position:"absolute",
    zIndex:1
  }


})
