import { StyleSheet, Dimensions } from 'react-native'
import Colors from '../../Themes/Colors'


export default StyleSheet.create({
  container: {
    flex:1,
    padding:50,
    borderRightWidth:1,
    borderColor:Colors.frost,
    width:Dimensions.get('window').width/3

  }
})
