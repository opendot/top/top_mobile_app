import { StyleSheet, Dimensions } from 'react-native'


export default StyleSheet.create({
  container: {
    backgroundColor:"#333333",
    position:"relative",
    borderWidth:1,
    borderColor:"#ff0000",
    height:Dimensions.get("window").height * 0.8,
    width:Dimensions.get("window").height * 0.8 * (16/9)
  }
})
