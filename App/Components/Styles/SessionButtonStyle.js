import { StyleSheet } from 'react-native'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
  container: {
    flex: 1,
    height: "100%",
    borderRightWidth:1,
    borderRightColor:Colors.frost,
    flexDirection:"column",
    justifyContent:"space-around",
    textAlign:"center",
    alignItems:"center"
  }
})
