import { StyleSheet } from 'react-native'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
  container: {
    flex: 3,
    flexDirection:'row'
  },
  gameTypeContainer:{
    flex:1,
    flexDirection:"column",
    justifyContent:"space-around",
    alignItems:"flex-start",
    paddingTop:40,
  },
  gameList:{
    flex:1,
    borderLeftWidth:1,
    borderColor:Colors.frost,
    flexDirection:"column",
    justifyContent:"space-around",
    alignItems:"flex-start",
    paddingTop:40
  }
})
