import { StyleSheet } from 'react-native'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
  container: {
    position:'absolute',
    width:10,
    height:10,
    backgroundColor:Colors.darkred,
    borderRadius:5,
    zIndex:3
  }
})
