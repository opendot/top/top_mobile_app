import { Dimensions, StyleSheet } from 'react-native'
import Colors from '../../Themes/Colors'
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

export default StyleSheet.create({
  container: {
    position:"absolute",
    top:width*0.8*(9/16),
    left:0,
    width:width*0.8,
    height:height - (width*0.8)*(9/16),
    paddingBottom:30,
    backgroundColor:"#ffffff",
    zIndex:3,
    borderWidth: 1,
    borderColor: Colors.frost,
    flexDirection:'row'
  }

})
