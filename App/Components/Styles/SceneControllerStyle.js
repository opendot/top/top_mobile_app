import { Dimensions, StyleSheet } from 'react-native'
import Colors from '../../Themes/Colors'
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

export default StyleSheet.create({
  container: {
    position:"absolute", height:height, top:0, left:width*0.8,width:width*0.2,backgroundColor:"#ffffff", zIndex:1, borderLeftWidth:1, borderColor:Colors.frost, alignItems:"center", flexDirection:"column", flex:1
  },
  nextback:{
  flex:1,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"space-between",
    textAlign:"center",
    padding:10,
    width:"40%",
    height:40,
    borderWidth: 1,
    borderColor:"black",
    borderRadius:10,
    margin:3
},
nextback_dis:{
  opacity:0.2,
    flex:1,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"space-between",
    textAlign:"center",
    padding:10,
    width:"40%",
    height:40,
    borderWidth: 1,
    borderColor:"rgba(0,0,0,0.2)",
    borderRadius:10,
    margin:3
}
})
