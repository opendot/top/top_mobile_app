import { StyleSheet } from 'react-native'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
  container: {
    width:'100%',
    height:70,
    paddingTop:5,
    paddingBottom:5,
    paddingLeft:5,
    borderBottomWidth:1,
    borderColor:Colors.frost
  }
})
