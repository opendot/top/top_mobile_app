import { StyleSheet } from 'react-native'
export default StyleSheet.create({
  container: {
    width:'80%',
    paddingTop:5,
    paddingBottom:5,
    paddingLeft:5
  }
})
