import { StyleSheet } from 'react-native'
import { Fonts, Colors, Metrics } from '../../Themes/'

export default StyleSheet.create({
  button: {
    height: 45,
    borderRadius: 10,
    marginHorizontal: Metrics.section,
    marginVertical: Metrics.baseMargin,
    //backgroundColor: Colors.text,
    borderColor: Colors.brand,
    borderWidth:1,
    justifyContent: 'center'
  },
  buttonText: {
    color: Colors.panther,
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: Fonts.size.medium,
    marginVertical: Metrics.baseMargin
  }
})
