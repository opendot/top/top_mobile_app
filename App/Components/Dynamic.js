import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text } from 'react-native'
import styles from './Styles/DynamicStyle'
import Slider from "@react-native-community/slider"
import Colors from '../Themes/Colors'

export default class Dynamic extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    return (
      <View style={{marginTop: 10}}>
        <Text>{this.props.name}</Text>
        <View style={{flex:1, flexDirection:"row", paddingTop:10,alignItems:"center", justifyContent:"space-between"}}>
          <Text style={{color:Colors.panther,fontSize:10}}>{this.props.min}</Text>
          <Text style={{color:Colors.brand,fontSize:12, fontWeight:'bold'}}>{this.props.value}</Text>
          <Text style={{color:Colors.panther,fontSize:10}}>{this.props.max}</Text>
        </View>
        <Slider
          style={{width: this.props.width, height: 40}}
          minimumValue={this.props.min}
          maximumValue={this.props.max}
          step={this.props.step}
          minimumTrackTintColor={Colors.brand}
          maximumTrackTintColor={Colors.frost}
          onValueChange={this.props.onSlideEnd}
          value={this.props.value}
          thumbTouchSize={{width: 50, height: 40}}
        />

      </View>
    )
  }
}
