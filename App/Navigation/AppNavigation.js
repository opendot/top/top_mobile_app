import { createStackNavigator, createAppContainer } from 'react-navigation'
import CalibrationScreen from '../Containers/CalibrationScreen'
import GameDetailScreen from '../Containers/GameDetailScreen'
import WelcomeScreen from '../Containers/WelcomeScreen'
import PatientsScreen from '../Containers/PatientsScreen'
import MainScreen from '../Containers/MainScreen'
import LoginScreen from '../Containers/LoginScreen'
import QrScreen from '../Containers/QrScreen'
import SessionScreen from '../Containers/SessionScreen'

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = createStackNavigator({
  CalibrationScreen: { screen: CalibrationScreen },
  GameDetailScreen: { screen: GameDetailScreen },
  WelcomeScreen: { screen: WelcomeScreen },
  PatientsScreen: { screen: PatientsScreen },
  MainScreen: { screen: MainScreen },
  LoginScreen: { screen: LoginScreen },
  QrScreen: { screen: QrScreen },
  SessionScreen: { screen: SessionScreen }
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'WelcomeScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default createAppContainer(PrimaryNav)
