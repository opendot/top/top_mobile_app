import React, { Component } from 'react'
import { ScrollView, Text, TouchableOpacity, View, TextInput, Dimensions } from 'react-native'
import { connect } from 'react-redux'
import ConnectionActions from '../Redux/ConnectionRedux'
import { ConnectionSelectors } from '../Redux/ConnectionRedux'


// Styles
import styles from './Styles/PatientsScreenStyle'
import Colors from '../Themes/Colors'
import { NavigationActions, StackActions } from 'react-navigation'


class PatientsScreen extends Component {


  componentDidMount (): void {
    console.log("entering patients screen")
    if(this.props.user.careReceiver !== undefined) this.navigateToMainActivity()
    this.props.getAllPatients()
  }

  navigateToMainActivity() {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({routeName: 'MainScreen'}),
      ],
    });
    this.props.navigation.dispatch(resetAction);
  }

  selectPatient =(y) => {
    this.props.setPatient(y);
    this.navigateToMainActivity()
}


  render () {
    return (
      <View style={{flex:1}}>
        <Text style={{width:"100%",height:60, backgroundColor:Colors.brand, color:Colors.text, textAlign: 'center', fontSize:20, padding:15}}>
          Seleziona un care receiver
        </Text>
        <View style={{flex:1, flexWrap:'wrap', flexDirection: 'row',justifyContent: 'flex-start', alignItems:'flex-start'}}>
          {
            this.props.patients && this.props.patients.map((y) => {
              return (
                <TouchableOpacity
                style={{width:Dimensions.get('window').width/6-40, margin:20, padding:10, borderWidth:1, borderRadius:10, borderColor:Colors.brand}}
                key={y.id}
                onPress={() => {this.selectPatient(y)}}
                >
                <Text style={{textAlign:'center'}}>{y.name + " "+y.last_name}</Text>
                </TouchableOpacity>);
            })
          }
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {

  return {
    patients: ConnectionSelectors.getPatients(state),
    user: ConnectionSelectors.getUser(state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getAllPatients: () => dispatch(ConnectionActions.patientsRequest()),
    setPatient: (patient) => dispatch(ConnectionActions.setPatient(patient))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PatientsScreen)
