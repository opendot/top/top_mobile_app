import React, { Component } from 'react'
import { ScrollView,View, Text, Image, Switch, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import GameActions,{ GameSelectors } from '../Redux/GameRedux'
import TrackerSocketActions from '../Redux/TrackerSocketRedux'
// Styles
import styles from './Styles/GameDetailScreenStyle'
import Colors from '../Themes/Colors'
import { ConnectionSelectors } from '../Redux/ConnectionRedux'
import { TrackerSocketSelectors } from '../Redux/TrackerSocketRedux'
import _ from "lodash"
import Dynamic from '../Components/Dynamic'
import BooleanDynamic from '../Components/BooleanDynamic'


class GameDetailScreen extends Component {

  constructor(props){
    super(props)
    this.state = {
      switch:false
    }
}

drawScenes() {

  const rounds = _(this.props.levels).find(['index', this.props.selectedLevel]).rounds
  return rounds.map((o) => {
    console.log(o.name)
    return (
      <TouchableOpacity key={o.name} onPress={()=>{this.props.selectRound(o.index)}} style={o.index == this.props.selectedRound ? styles.activeRoundBox : styles.roundBox}>
        <Text style={o.index == this.props.selectedRound ? styles.activeSceneTitle : styles.sceneTitle}>{o.name}</Text>
        <View style={{padding: 15, paddingTop:7}}>
          <Text>{o.description}</Text>
        </View>
        <Image style={styles.roundImage} source={{uri:"http://"+this.props.ip+":"+this.props.port+this.props.info.url+"img/"+o.img}}></Image>
      </TouchableOpacity>
    )
  })
}

componentWillReceiveProps (nextProps, nextContext){
    if(!nextProps.sessionCorrect){
      setTimeout(()=>{
        this.props.sessionCorrectSuccess()
        console.log("correct is now true, hide message");
      }, 5000)
    }
}

  componentDidMount (): void {
   this.props.selectLevel(this.props.levels[0].index)
    this.props.selectRound(0)
    if(!this.props.sessionCorrect){
      console.log("set session correct to true to hide message");
      setTimeout(()=>{
        this.props.sessionCorrectSuccess()
        console.log("correct is now true, hide message");
      }, 5000)
    }
}

  render () {

    let recSessEnabled = !this.props.user.guest && this.props.isLicensed;

    return (
      <View style={{flex:1}}>
        <Text style={styles.screenTitle}>
          Dettagli del gioco
        </Text>
        <View style={styles.infoContainer}>
          <Image style={styles.gameImage} source={{uri:"http://"+this.props.ip+":"+this.props.port+this.props.info.image}}/>
          <View style={styles.gameInfo}>
            <View>
              <Text style={styles.gameTitle}>{this.props.info.title}</Text>
              <Text style={styles.gameDescription}>{this.props.info.description}</Text>
            </View>
          </View>
        </View>

        <View style={styles.commands}>
          <View style={styles.levelCont}>
            <Text>Seleziona un livello</Text>
            {this.props.levels.map((o)=>{
              return(
                <TouchableOpacity key={o.index} onPress={()=>{this.props.selectLevel(o.index); this.props.selectRound(0)}} style={[styles.touchLevel,
                  {backgroundColor: this.props.selectedLevel == o.index ? Colors.brand : "transparent"}]}>
                  <Text style={{
                    lineHeight:30,
                    textAlign:"center",
                    padding:3,
                    color: this.props.selectedLevel == o.index ? "#FFFFFF" : Colors.panther
                  }}>{o.index + 1}</Text>
                </TouchableOpacity>
              )
            })}
          </View>
        </View>

        <View style={styles.levelContainer}>
          <View style={styles.dynamics}>
            <Text style={{textAlign: 'left', fontSize: 14, fontWeight: 'bold'}}>Parametri</Text>
            {this.props.levels[this.props.selectedLevel].dynamics.filter(d => d.id !== 'backScene' && d.id !== 'nextScene').map((d) => {
              if (d.type=="number") return (
                <Dynamic onSlideEnd={(val)=>{this.props.updateDynamics(this.props.selectedLevel, d.id, this.props.levels[this.props.selectedLevel].index, val)}}
                         key={d.id}
                         width={200 - 30}
                         name={d.name}
                         min={d.min}
                         max={d.max}
                         step={d.step}
                         value={d.value}/>
              )
              else return(
                <BooleanDynamic
                  onSlideEnd={(val)=>{this.props.updateDynamics(this.props.selectedLevel, d.id, this.props.levels[this.props.selectedLevel].index, val)}}
                  key={d.id}
                  width={200 - 30}
                  name={d.name}
                  value={d.value}/>
              )
            })}
          </View>
          <View style={{width: "78%"}}>
            <Text style={{textAlign: 'left', fontSize: 14, fontWeight: 'bold', padding:10}}>Seleziona un round</Text>
            <ScrollView horizontal={true} styles={styles.roundSubcont}>
              {this.props.selectedLevel != null && this.drawScenes()}
            </ScrollView>
          </View>


        </View>
        <View style={{width:"100%",height:60, backgroundColor:Colors.text}}>
          <View style={styles.footer}>
            <TouchableOpacity onPress={()=>{this.props.setPreviewSession()}} style={{backgroundColor:Colors.brand, borderRadius:20, height:40,paddingLeft:10, paddingRight:10}}>
              <Text style={{color:Colors.text, lineHeight:40 }}>{recSessEnabled ? "Sessione di preview" : "Avvia sessione"}</Text>
            </TouchableOpacity>
            {recSessEnabled && <TouchableOpacity onPress={()=>{this.props.setRecordedSession()}} style={{backgroundColor:Colors.darkred, borderRadius:20, height:40, paddingLeft:10, paddingRight:10}}>
              <Text style={{color:Colors.text, lineHeight:40 }}>Sessione registrata</Text>
            </TouchableOpacity>}
          </View>
        </View>

        {!this.props.sessionCorrect && <View style={{flex:1,position:"absolute",alignItems:"center",justifyContent:"center",alignSelf:"center",bottom:10, width:90+"%",height:100, padding:20, borderRadius:10, backgroundColor: Colors.darkred}}>
          <Text style={{color:"#ffffff",fontSize:24}}>Errore nella registrazione della sessione</Text>
        </View>}
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    info:GameSelectors.getInfo(state),
    skipIntro:GameSelectors.getSkipIntro(state),
    details:GameSelectors.getDetails(state),
    selectedLevel: GameSelectors.getSelectedLevel(state),
    selectedRound: GameSelectors.getSelectedRound(state),
    sessionCorrect: GameSelectors.getSessionCorrect(state),
    levels:GameSelectors.getLevels(state),
    ip: ConnectionSelectors.getIp(state),
    port: ConnectionSelectors.getPort(state),
    user: ConnectionSelectors.getUser(state),
    isLicensed: TrackerSocketSelectors.isLicensed(state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setSkipIntro: (val) => dispatch(GameActions.setSkipIntro(val)),
    selectLevel: (lvl) => dispatch(GameActions.setSelectedLevel(lvl)),
    selectRound: (rnd) => dispatch(GameActions.setSelectedRound(rnd)),
    updateDynamics: (lvl, dyn_id, scene, val) => dispatch(GameActions.setDynamics(lvl, dyn_id, scene, val)),
    setPreviewSession: () => dispatch(GameActions.setPreviewSession()),
    setRecordedSession: () => dispatch(GameActions.newRecordedSession()),
    sessionCorrectSuccess: () => dispatch(GameActions.sessionCorrectSuccess())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GameDetailScreen)
