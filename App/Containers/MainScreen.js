import React, { Component } from 'react'
import { View, Text, KeyboardAvoidingView } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/MainScreenStyle'
import SideMenu from '../Components/SideMenu'
import GamesMenu from '../Components/GamesMenu'
import _ from "lodash"
import ConnectionActions, { ConnectionSelectors } from '../Redux/ConnectionRedux'
import TrackerSocketActions, { TrackerSocketSelectors } from '../Redux/TrackerSocketRedux'
import { YellowBox } from 'react-native';
import GameActions from '../Redux/GameRedux'
YellowBox.ignoreWarnings([
  'Unrecognized WebSocket connection option(s) `agent`, `perMessageDeflate`, `pfx`, `key`, `passphrase`, `cert`, `ca`, `ciphers`, `rejectUnauthorized`. Did you mean to put these under `headers`?'
]);

class MainScreen extends Component {

  componentDidMount() {
    this.props.getGames();
    console.log(this.props.connected);
    if(!this.props.connected) {
      this.props.connectTracker();
    }
    else{
      console.log("already connected")
    }
    this.props.selectLevel(0);
    this.props.setSkipIntro(false);
  }

  render () {
    return (
      <View style={styles.mainContainer}>
       <SideMenu evaluation={_.find(this.props.games, function(o) { return o.type == "evaluation"; })}/>
       <GamesMenu games={this.props.games}/>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    games:ConnectionSelectors.getGames(state),
    connected:TrackerSocketSelectors.isConnected(state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getGames: () =>  dispatch(ConnectionActions.gamesRequest()),
    selectLevel: (lvl) => dispatch(GameActions.setSelectedLevel(lvl)),
    connectTracker: () => dispatch(TrackerSocketActions.initSocketChannel()),
    setSkipIntro: (val) => dispatch(GameActions.setSkipIntro(val)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen)
