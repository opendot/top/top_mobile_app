import React, { Component } from 'react'
import { View, Text, KeyboardAvoidingView } from 'react-native'
import { connect } from 'react-redux'

// Styles
import styles from './Styles/CalibrationScreenStyle'
import CalibrationMonitor from '../Components/CalibrationMonitor'
import CalibrationManager from '../Components/CalibrationManager'
import TrackerSocketActions, { TrackerSocketSelectors } from '../Redux/TrackerSocketRedux'
import Colors from '../Themes/Colors'
import { NavigationActions, StackActions } from 'react-navigation'

class CalibrationScreen extends Component {


  navigateToMainActivity() {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({routeName: 'MainScreen'}),
      ],
    });
    this.props.navigation.dispatch(resetAction);
  }

  backToMenu = () => {
    this.props.sendTrackerWs({
      type: "interrupt_calibration",
      data:1
    })

    this.navigateToMainActivity()
  }

  render () {
    return (
      <View style={{flex:1}}>
        <View style={{flex:8, backgroundColor:"#FFFFFF", justifyContent:"center", alignItems:"center"}}>

          <CalibrationMonitor
            points = {this.props.calibrationPoints}
            currPoint = {this.props.currentPoint}
            calibrating = {this.props.isCalibrating}
            gaze = {this.props.gaze}
          >

        </CalibrationMonitor>
        </View>

        <View style={{flex:2, backgroundColor:"#FFFFFF", borderTopWidth: 1, borderTopColor: Colors.frost}}>

          <CalibrationManager
            calibrating = {this.props.isCalibrating}
            onBack = {this.backToMenu}
          >
          </CalibrationManager>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    calibrationPoints: TrackerSocketSelectors.getCalibrationPoints(state),
    currentPoint: TrackerSocketSelectors.getCurrentPoint(state),
    isCalibrating: TrackerSocketSelectors.isCalibrating(state),
    gaze:TrackerSocketSelectors.getGaze(state),
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    sendTrackerWs: (data) => dispatch(TrackerSocketActions.sendToTrackerWs(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CalibrationScreen)
