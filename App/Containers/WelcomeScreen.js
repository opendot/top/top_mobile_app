import React, { Component } from 'react'
import { Text, View, Image } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/WelcomeScreenStyle'
import FullButton from '../Components/FullButton'
import ConnectionActions,{ ConnectionSelectors } from '../Redux/ConnectionRedux'
import TrackerSocketActions from '../Redux/TrackerSocketRedux'
import Colors from '../Themes/Colors'

class WelcomeScreen extends Component {

  componentDidMount (){

  }

  render () {
    return (
      <View style={styles.welcomeHome}>
        <Image 
          style={styles.logo}
          source={require('../Images/TOP_logo_transparent.png')}
        />
        <View style={{flex:1, flexDirection:'row',alignItems:'center',justifyContent:'space-around'}}>
          <FullButton 
            styles={{ borderRadius:10, margin:20, minWidth: '25%', height: 80, backgroundColor: Colors.brand, display:'flex', justifyContent: 'center'}} 
            textStyles={{fontSize: 25}}
            text={'Inquadra QR code'} 
            onPress={()=>{
              this.props.setConnected(false)
              this.props.navigation.navigate('QrScreen')}} />
          {(this.props.user && this.props.ip) &&
          <FullButton 
            styles={{borderRadius:10,margin:20, minWidth: '25%', height: 80, borderColor: Colors.brand, display:'flex', justifyContent: 'center'}} 
            textStyles={{fontSize: 25, color: Colors.brand}}
            text={"Usa ultimo login"} 
            onPress={()=>{
              if (this.props.currentPatient) {
                this.props.changeBaseUrl(this.props.ip, this.props.port)
                this.props.setConnected(false)
                this.props.navigation.navigate('MainScreen')
              } else {
                this.props.setConnected(false)
                this.props.navigation.navigate('PatientsScreen')
              }
              }} />
          }
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    user:ConnectionSelectors.getUser(state),
    ip:ConnectionSelectors.getIp(state),
    port:ConnectionSelectors.getPort(state),
    currentPatient: ConnectionSelectors.getCurrPatient(state),
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    changeBaseUrl: (ip, port) => dispatch(ConnectionActions.changeBaseurl(ip,port)),
    setConnected: (val) => dispatch(TrackerSocketActions.setConnected(val))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WelcomeScreen)
