import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { connect } from 'react-redux'
import ConnectionActions from '../Redux/ConnectionRedux'

// Styles
import styles from './Styles/QrScreenStyle'
import QRCodeScanner from 'react-native-qrcode-scanner';
import Colors from '../Themes/Colors'
import _ from "lodash"
class QrScreen extends Component {

  static navigationOptions = {
    title: 'Inquadra il codice QR',
    headerStyle: {
      backgroundColor: '#f4511e',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };


  onSuccess(e) {
    console.log("successful read")
    console.log(e.data)
    let split = e.data.split(":")
    let ip = split[0]
    let port = split[1]
    console.log("ip and port",ip, port)
    this.props.checkAddress(ip, port)
  }

  render () {
    return (
      <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
      <Text style={{width:"100%",height:60, backgroundColor:Colors.brand, color:Colors.text, textAlign: 'center', fontSize:20, padding:15}}>
        Inquadra il codice QR
      </Text>


      <QRCodeScanner
        onRead={this.onSuccess.bind(this)}
        showMarker={true}
        containerStyle={{flex:1, alignItems: 'center',backgroundColor: Colors.charcoal}}
        cameraStyle={{ marginTop:60,width:400,height:400 }}
      />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {

  }
}

const mapDispatchToProps = (dispatch) => {
  return {

    checkAddress: (ip, port) => dispatch(ConnectionActions.checkAddress(ip, port))

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(QrScreen)
