import { StyleSheet } from 'react-native'
import { ApplicationStyles } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  mainContainer:{
    flex:1,
    flexDirection:"row",
    justifyContent:"flex-start"
  }
})
