import { StyleSheet } from 'react-native'
import { ApplicationStyles } from '../../Themes/'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  infoContainer:{
    flex:2,
    flexDirection:"row",
    padding:10,
    borderBottomWidth:1,
    borderBottomColor:Colors.frost,
    shadowColor: Colors.frost,
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  levelContainer:{
    flex:5,
    flexDirection:"row",
    backgroundColor:"#eeeeee",
    borderWidth:1,
    borderColor:Colors.frost
  },
  gameTitle:{
    fontSize:24, fontWeight:'700'
  },
  gameDescription:{
    fontSize:14, fontWeight:'400'
  },
  gameImage:{
    width:120,height:120
  },
  gameInfo:{
    width:"80%",paddingLeft:30,paddingRight:10
  },
  screenTitle:{
    width:"100%",height:60, backgroundColor:Colors.brand, color:Colors.text, textAlign: 'center', fontSize:20, padding:15
  },
  dynamics:{
    width: "22%",
    padding:10,
    backgroundColor:"#ffffff",
    marginRight:5,
    borderRightColor:Colors.frost,
    borderRightWidth:1
  },
  roundSubcont:{
    flex:1,
   paddingRight:20
  },
  roundImage:{
    position:"relative",
    bottom:0,
    left:0,
    flex:1
  },
  activeRoundBox:{marginRight:10, marginLeft:10, marginBottom:10,backgroundColor:"#ffffff", width: 200, borderWidth: 1, borderColor: Colors.brand},
  roundBox:{marginRight:10, marginLeft:10, marginBottom:10,backgroundColor:"#ffffff", width: 200, borderWidth: 1, borderColor: Colors.frost},
  commands:{flex:1, flexDirection:'row', justifyContent:"space-around", alignItems:"center"},
  levelCont:{flex:1, flexDirection:'row', alignItems:"center",justifyContent:"center"},
  touchLevel:{width:30, height:30,justifyContent:"center", alignItems:"center",marginLeft:8, borderWidth:1, borderColor:Colors.brand, borderRadius:15},
  introCont:{flex:1, flexDirection:'row', alignItems:"center", justifyContent:"flex-start"},
  activeSceneTitle:{width: "100%", height: 35, borderColor: Colors.brand, borderWidth: 1, backgroundColor: Colors.brand, color: Colors.text, padding: 5, textAlign: 'center', marginBottom: 10, fontSize: 18, fontWeight: 'bold'},
  sceneTitle:{width: "100%", height: 35, borderColor: Colors.frost, borderWidth: 1, backgroundColor: Colors.frost, color: Colors.text, padding: 5, textAlign: 'center', marginBottom: 10, fontSize: 18, fontWeight: 'bold'},
  footer:{flex: 1, flexDirection: 'row', justifyContent: "space-around", alignItems: "center", shadowColor: Colors.frost},
})
