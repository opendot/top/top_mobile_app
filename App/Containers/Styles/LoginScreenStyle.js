import { StyleSheet } from 'react-native'
import { ApplicationStyles } from '../../Themes/'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  loginTitle:{
    color:Colors.brand,
    fontWeight:'bold',
    fontSize:20,
    marginBottom:10,
    textAlign: 'center',
  },
  loginCred:{
    color:Colors.panther,
    fontWeight:'bold',
    fontSize:20,
    marginBottom:30,
  }
})
