import { StyleSheet } from 'react-native'
import { ApplicationStyles } from '../../Themes/'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  welcomeHome:{
    flex:1,
    backgroundColor:Colors.backgroundLight,
    color:Colors.text,
    alignItems: 'center',
    justifyContent:'center'
  },
  logo: {
    flex: 1,
    width: '20%',
    resizeMode: 'contain'
  }
})
