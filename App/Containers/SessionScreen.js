import React, { Component } from 'react'
import { ScrollView, Text, Image, View, TouchableOpacity, Dimensions } from 'react-native'
import { DeepDiff } from 'deep-diff';
import _ from "lodash";

// Styles
import styles from './Styles/SessionScreenStyles'
import SceneBuilder from '../Components/SceneBuilder'
import { connect, Provider } from 'react-redux'
import { GameSelectors } from '../Redux/GameRedux'
import SceneController from '../Components/SceneController'
import { ConnectionSelectors } from '../Redux/ConnectionRedux'
import SessionController from '../Components/SessionController'
import KeepAwake from 'react-native-keep-awake';
import TrackerSocketActions from '../Redux/TrackerSocketRedux'

class SessionScreen extends Component {

  constructor (props) {
    super(props);
    this.state = {
      currScene: 0,
      currTarget: null,
      closing:false,
      render: {res: [1920, 1080]},
      buttons: {cooldown:true, next:true, back:true}
    };
  }

  selectScene (scn) {
    this.setState({currScene: scn})
    /*if(scn.targets && scn.targets.length){
      this.setState({currTarget: scn.targets[0].name})
    }*/
  }


  connectEventSocket(){
    this.socket = new WebSocket("ws://"+this.props.ip+":5002");

    this.socket.onopen = () => {
      console.log("connected to events client");
      this.socket.send(JSON.stringify({type:"room", data:"mobile"}))
    }

    this.socket.onerror = (msg) => {
      console.log("error on events client", msg);
    }

    this.socket.onclose = () => {
      if(!this.closing){
        setTimeout(()=>{this.connectEventSocket(), 1000})
      }
    }

    this.socket.onmessage = (message) => {
      let msg = JSON.parse(message.data)
      if(msg.type == "render"){
        this.parseDiff(msg.data);
      }
      else if(msg.type == "event"){
        switch (msg.event_type){
          case "change_scene":
            this.setState({currScene: msg.data})
            break;

          case "buttons_state":
            console.log("change buttons|", msg)
            this.setState({buttons: msg.data})
            break;
        }
      }
    };
  }

  componentDidMount () {
    let lvl = _(this.props.levels).find((l) => {return l.index == this.props.selectedLevel})
    let scns = lvl.scenes
    let scn = _(scns).find((s) => {return s.index == this.state.currScene})
    let dynamicsToSend = [];

      lvl.dynamics.forEach( d => {
        if(d.id !== 'backScene' && d.id !== 'nextScene'){
          dynamicsToSend.push(d);
        }
      });

    this.props.sendTrackerWs({type:"new_session",data:{gameurl:this.props.info.url, round: this.props.selectedRound, session:this.props.session,level:this.props.selectedLevel, skipIntro: this.props.skipIntro, dynamics:dynamicsToSend}})

    if(scn.targets && scn.targets.length){
      this.setState({currTarget: scn.targets[0].name})
    }

    this.connectEventSocket()
    //this.loadAssets()

  }

  componentWillUnmount() {
    // TODO: Remove on listener
    this.closing = true
    this.socket.close();
  }
/*
  loadAssets(){
    let baseurl='http://' + this.props.ip + ":" + this.props.port
    baseurl+=this.props.info.url +"img/"
    this.props.details.ingame_assets.forEach((p)=>{
      Image.prefetch(baseurl+p.name)
    })
  }
*/

  componentDidUpdate(prevProps, prevState, snapshot){
    if(prevState.currScene != this.state.currScene){
      let scn = this.currentScene();
      if(scn.targets && scn.targets.length){
        this.setState({currTarget: scn.targets[0].name})
      }
    }
  }

  sessionButtonTouch = (obj) => {
    this.socket.send(obj);
  }

  changeDynamics = (obj) => {
    this.socket.send(JSON.stringify({
      type: "event",
      event_type: "change_dynamics",
      data: {id: obj.id, value: obj.value}
    }))
  }


  onTouch  = (type,obj)=>{
    switch(type) {
      case "create":
        this.socket.send(JSON.stringify({
            type: "event",
            event_type: "add_target",
            data: {x: obj.x, y: obj.y, type: this.state.currTarget}
          }))
        break;

      case "touch":
        this.socket.send(JSON.stringify({
          type: "event",
          event_type: "remove_target",
          data: {id:obj.id}
        }))
        break;

      case "move":

        this.socket.send(JSON.stringify({
          type: "event",
          event_type: "move_target",
          data: {id:obj.id, x:obj.x, y:obj.y}
        }))
        break;

      default:
        break;
    }
  }


  toggleFatigue = () => {
    console.log('back scene');
    this.socket.send(JSON.stringify({
      type: "event",
      event_type: "toggle_fatigue",
      data: {}
    }));
  }

  backScene = () => {
    console.log('back scene');
    this.socket.send(JSON.stringify({
      type: "event",
      event_type: "back_scene",
      data: {}
    }));
  }
  nextScene = () => {
    console.log('next scene');
    this.socket.send(JSON.stringify({
      type: "event",
      event_type: "next_scene",
      data: {}
    }));
  }

  parseDiff = (msg) => {
    this.setState({render: msg});
  }

  selectarget = (tgt) => {
    this.setState({currTarget: tgt})
  }

  currentScene = () => {
    let lvl = _(this.props.levels).find((l) => {return l.index == this.props.selectedLevel}).scenes
    let scn = _(lvl).find((s) => {return s.index == this.state.currScene})
    return scn
  }
  setBackOrNext = direction =>{
    return this.currentScene().dynamics.filter(d => d.id == direction).length ? 'flex' : 'none'
  }
  render () {
    return (
      <View style={styles.mainContainer}>
        <KeepAwake />
        {
          this.state.render.res &&
          <SceneBuilder
            baseurl={'http://' + this.props.ip + ":" + this.props.port}
            send={this.onTouch}
            background={this.state.render.bg}
            game={this.props.info}
            details = {this.props.details}
            targets={_.values(this.state.render.targets) || []}
            distractors={_.values(this.state.render.distractors) || []}
            res={this.state.render.res}/>}

        <SceneController
          selTarget={this.state.currTarget}
          onTargetPress={(t) => {this.setState({currTarget: t})}}
          baseurl={'http://' + this.props.ip + ":" + this.props.port}
          game={this.props.info}
          onChangeDynamics = {this.changeDynamics}
          onBack = {this.backScene}
          onNext = {this.nextScene}
          fatigue = {this.toggleFatigue}
          buttons = {this.state.buttons}
          scene = {this.state.currScene}/>

        <SessionController
          send = {this.sessionButtonTouch}
          session = {this.props.session}
          />
      </View>
    )
  }
}


const mapStateToProps = (state) => {
  return {
    skipIntro: GameSelectors.getSkipIntro(state),
    session: GameSelectors.getSession(state),
    levels: GameSelectors.getLevels(state),
    details: GameSelectors.getDetails(state),
    info: GameSelectors.getInfo(state),
    selectedLevel: GameSelectors.getSelectedLevel(state),
    selectedRound: GameSelectors.getSelectedRound(state),
    ip: ConnectionSelectors.getIp(state),
    port: ConnectionSelectors.getPort(state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    sendTrackerWs: (data) => dispatch(TrackerSocketActions.sendToTrackerWs(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SessionScreen)
