import React, { Component } from 'react'
import { View,TextInput, Text, ScrollView } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/LoginScreenStyle'
import Colors from '../Themes/Colors'
import RoundedButton from '../Components/RoundedButton'
import ConnectionActions from '../Redux/ConnectionRedux'

class LoginScreen extends Component {

  constructor(props) {
    super(props);
    this.state = { username: '', password:'' };
  }


  render () {
    return (
      <View style={{flex:1}}>
        <Text style={{width:"100%",height:60, backgroundColor:Colors.brand, color:Colors.text, textAlign: 'center', fontSize:20, padding:15}}>
          Esegui il login
        </Text>
     <ScrollView contentContainerStyle={{flex:1, flexDirection: 'row',justifyContent: 'space-around', alignItems:'center'}}>
        <View style={{padding:30, borderWidth:1, borderRadius:10, borderColor:Colors.panther}}>
          <Text style={styles.loginTitle}>Accesso Terapisti</Text>
          <Text style={styles.loginCred}>Inserisci le tue credenziali</Text>
          <TextInput
            style={{height: 40, borderColor: Colors.brand, borderWidth: 1, marginBottom: 5}}
            onChangeText={(username) => this.setState({username})} keyboardType={'email-address'}
            value={this.state.username}
          />
          <TextInput
            style={{height: 40, borderColor: Colors.brand, borderWidth: 1, marginBottom:30}}
            onChangeText={(password) => this.setState({password})} secureTextEntry={true}
            value={this.state.password}
          />
          <RoundedButton text={"Accedi"} onPress={()=>{this.props.doLogin(this.state.username.toLowerCase(), this.state.password)}}/>
        </View>
     </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {

  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    doLogin: (username,password) => dispatch(ConnectionActions.loginRequest(username, password))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
