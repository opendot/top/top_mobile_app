/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put, select } from 'redux-saga/effects'
import ConnectionActions, { ConnectionSelectors } from '../Redux/ConnectionRedux'
import { NavigationActions } from 'react-navigation';
import _ from "lodash"
import TrackerSocketActions from '../Redux/TrackerSocketRedux';
import Config from 'react-native-config';


export function * changeBaseurl(api, action){
  const { ip, port } = action
  api.changeBaseurl("http://"+ip+":"+port)
}

export function * getAddress (api, action) {
  const { ip, port } = action
  api.changeBaseurl("http://"+ip+":"+port)
  const response = yield call(api.getPing)

  // success?
  if (response.ok) {
    yield put(ConnectionActions.storeAddress(ip,port))
    if(Config.APP_VERSION == 'family'){
      yield put(ConnectionActions.loginRequest('guest', 'guest'))
    }else{
      yield put(NavigationActions.navigate({ routeName: 'LoginScreen' }));
    }
  } else {
    yield put(ConnectionActions.wrongAddress())
  }
}

export function * login(api,action){
  const { username, password } = action
  console.log("requesting login", username, password)
  const response = yield call(api.postLogin,username,password)

  // success?
  if (response.ok) {
    console.log("login ok")
    yield put(ConnectionActions.loginSuccess(response.data))
    yield put(NavigationActions.navigate({ routeName: Config.APP_VERSION == 'family' ? 'MainScreen' : 'PatientsScreen' }));
  } else {
    console.log("login ko")
    console.log(response);
    yield put(ConnectionActions.loginFailure())
  }
}

export function * getPatients(api,action){
  console.log("requesting patients")
  const token = yield select(ConnectionSelectors.getToken)
  console.log(token);
  const response = yield call(api.getPatients,token)

  // success?
  if (response.ok) {
    console.log("patients ok")
    yield put(ConnectionActions.patientsSuccess(response.data))
  } else {
    console.log("patients ko")
    console.log(response);
    yield put(ConnectionActions.patientsFailure())
  }
}

export function * getGames(api,action){
  console.log("requesting games")
  //const token = yield select(ConnectionSelectors.getToken)
  const token = yield select(ConnectionSelectors.getToken)
  console.log(token);
  const response = yield call(api.getGames,token)

  // success?
  if (response.ok) {
    console.log("games ok")
    const res = _(response.data)
      .orderBy(['safename', 'version'], ['asc', 'desc'])
      .uniqBy('safename')
      .value();
    yield put(ConnectionActions.gamesSuccess(res))
  } else {
    console.log("games ko")
    yield put(ConnectionActions.gamesFailure())
  }
}

export function * logout(){
  yield put(NavigationActions.navigate({ routeName: 'LoginScreen' }));
}

export function * startCalibration(){
  yield put(TrackerSocketActions.sendToTrackerWs({type: "run_calibration"}))
  yield put(NavigationActions.navigate({ routeName: 'CalibrationScreen' }));
}

export function * changePatient(){
  yield put(NavigationActions.navigate({ routeName: 'PatientsScreen' }));
}
