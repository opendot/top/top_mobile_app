import { takeLatest, all } from 'redux-saga/effects'
import API from '../Services/Api'
import FixtureAPI from '../Services/FixtureApi'
import DebugConfig from '../Config/DebugConfig'


/* ------------- Sagas ------------- */

import { getAddress, login, getPatients, changePatient, logout, getGames, changeBaseurl, startCalibration } from './ConnectionSagas'
import { getGameDetail, goToDetail, setPreviewGameSession, newRecordedGameSession, checkCorrectSession } from './GameSagas'
import { initializeWebSocketsChannel } from './TrackerSocketSagas'
import { ConnectionTypes } from '../Redux/ConnectionRedux'
import { GameTypes } from '../Redux/GameRedux'
import { TrackerSocketTypes } from '../Redux/TrackerSocketRedux'

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = DebugConfig.useFixtures ? FixtureAPI : API.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    // some sagas only receive an action
    //takeLatest(StartupTypes.STARTUP, startup),
    takeLatest(ConnectionTypes.CHANGE_BASEURL, changeBaseurl, api),
    takeLatest(ConnectionTypes.CHECK_ADDRESS, getAddress, api),
    takeLatest(ConnectionTypes.LOGIN_REQUEST, login, api),
    takeLatest(ConnectionTypes.PATIENTS_REQUEST, getPatients, api),
    takeLatest(ConnectionTypes.CHANGE_PATIENT, changePatient),
    takeLatest(ConnectionTypes.GAMES_REQUEST, getGames, api),
    takeLatest(ConnectionTypes.LOGOUT, logout),
    takeLatest(ConnectionTypes.START_CALIBRATION, startCalibration),
    takeLatest(GameTypes.SET_GAME_INFO, getGameDetail,api),
    takeLatest(GameTypes.SET_PREVIEW_SESSION, setPreviewGameSession),
    takeLatest(GameTypes.NEW_RECORDED_SESSION, newRecordedGameSession,api),
    takeLatest(GameTypes.CHECK_SESSION_CORRECT, checkCorrectSession,api),
    takeLatest(TrackerSocketTypes.INIT_SOCKET_CHANNEL, initializeWebSocketsChannel),


    // some sagas receive extra parameters in addition to an action
    //takeLatest(GithubTypes.USER_REQUEST, getUserAvatar, api)
  ])
}
