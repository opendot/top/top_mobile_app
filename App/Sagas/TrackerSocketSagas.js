/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { eventChannel } from 'redux-saga';
import { select, fork, take, call, put, cancel,race,all } from 'redux-saga/effects';
import TrackerSocketActions from '../Redux/TrackerSocketRedux'
import ConnectionActions, { ConnectionSelectors } from '../Redux/ConnectionRedux'

let updateTimeout;

function* createEventChannel(mySocket) {
  return eventChannel(emit => {

    mySocket.onopen = (message) => {
      console.log("connected on trackerws");
      mySocket.send(JSON.stringify({type:"room",data:"mobile"}))
      emit({type:"connected"})
    }

    mySocket.onclose = (message) => {
      console.log("trackerws closed");
      setTimeout(()=>{emit({type:"disconnected"})},500)
    }

    mySocket.onerror = (message) => {
      console.log("trackerws error", message);
    }

    mySocket.onmessage = (message) => {

      const msg = JSON.parse(message.data)
      emit(msg)
      if(msg.type == "gaze"){
        if(updateTimeout) clearTimeout(updateTimeout);
        updateTimeout = setTimeout(()=>{emit({...msg, data:null})}, 100)
      }
    };

    return () => {
      mySocket.close();
    };
  });
}

function* backgroundTask(socketChannel) {
  while (true) {
    const msg = yield take(socketChannel);
    if(msg.type == "gaze"){
      yield put(TrackerSocketActions.setTrackerData({data: msg.data}));
    }

    else if (msg.type == "connected"){
      console.log("setting as connected");
      yield put(TrackerSocketActions.setConnected(true));
    }

    else if (msg.type == "disconnected"){
      yield put(TrackerSocketActions.setConnected(false));
      yield put(TrackerSocketActions.stopWebsocket());
    }

    else if (msg.type == "calibrate"){
      yield put(TrackerSocketActions.setCalibrationPoints(msg.data));
      yield put(TrackerSocketActions.setCalibrating({calibrating: true}));
    }

    else if (msg.type == "calibrate_point"){
      yield put(TrackerSocketActions.setCurrentPoint(msg.data));
    }

    else if (msg.type == "recalibrate"){
      yield put(TrackerSocketActions.setCalibrationPoints(msg.data));
    }

    else if (msg.type == "end_calibration"){
      yield put(TrackerSocketActions.setCalibrating(false));
    }

    else if(msg.type == "license"){
      yield put(TrackerSocketActions.setLicensed(msg.data))
    }

  }
}

/*  User Created Message (e.g. dispatch({ type: 'EXE_TASK', taskid: 5 })) sent to ws server  */
function* executeTask(socketChannel) {
  while (true) {
    const data = yield take("SEND_TO_TRACKER_WS");
    console.log("sending!", data)
    socketChannel.send(JSON.stringify(data.data));
  }
}

export function* initializeWebSocketsChannel() {
  const ip = yield select(ConnectionSelectors.getIp)
  console.log("connecting");
  const mySocket = new WebSocket("ws://"+ip+":5001");
  const channel = yield call(createEventChannel, mySocket);
  while (true) {

    const { cancel } = yield race({
      task: all([
        call(backgroundTask, channel),
        call(executeTask, mySocket)
      ]),
      cancel: take("STOP_WEBSOCKET")
    });
    if (cancel) {
      // console.log('channel cancelled');
      channel.close();
      console.log("channel closed, reopening");
      yield put(TrackerSocketActions.initSocketChannel());
    }
  }
}
