/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put, select } from 'redux-saga/effects'
import GameActions from '../Redux/GameRedux'
import { NavigationActions } from 'react-navigation';
import { GameSelectors } from '../Redux/GameRedux'
import { ConnectionSelectors } from '../Redux/ConnectionRedux'

export function * goToDetail() {
  yield put(NavigationActions.navigate({ routeName: 'GameDetailScreen' }))
}

export function * setPreviewGameSession(api,cation) {
  yield put(NavigationActions.navigate({ routeName: 'SessionScreen' }))
}

export function * getGameDetail (api, action) {
  const info = yield select(GameSelectors.getInfo)
  const response = yield call(api.getGame, info)

  // success?
  if (response.ok) {
    console.log("details ok")
    yield put(GameActions.gameSuccess(response.data ))
    yield put(NavigationActions.navigate({ routeName: 'GameDetailScreen' }))
  } else {
    console.log("details ko")
    yield put(GameActions.gameFailure())
  }
}

export function * checkCorrectSession (api, action) {
  const session = yield select(GameSelectors.getSession)
  console.log("check if session is correct: ", session)
  const response = yield call(api.getSessionCorrect, session)

  // success?
  if (response.ok) {
    console.log("session is correct")
    yield put(GameActions.sessionCorrectSuccess())
  } else {
    console.log("session is wrong")
    yield put(GameActions.sessionCorrectFail())
  }
}


export function * newRecordedGameSession (api) {
  const info = yield select(GameSelectors.getInfo)
  const token = yield select(ConnectionSelectors.getToken)
  const user = yield select(ConnectionSelectors.getUser)
  const patient = yield select(ConnectionSelectors.getCurrPatient)
  const response = yield call(api.newSession, info, user, patient, token)

  // success?
  if (response.ok) {
    console.log("session ok")
    yield put(GameActions.recordedSessionSuccess(response.data ))
    yield put(NavigationActions.navigate({ routeName: 'SessionScreen' }))
  } else {
    console.log("session ko")
    yield put(GameActions.recordedSessionFail())
  }
}
